#include <iostream>
#include <map>
#include <algorithm>
#include "structMap.h"

using namespace std;

structMap::structMap(){}

bool structMap::addFromFile(long long price, long long amount){
    auto it = myMap.find(price);

    //if such a price already exists
    if (it != myMap.end()) {
        return false;
    }
    //if doesn't exist
    else {
        pair<long long, long long> newPair = make_pair(price, amount);
        myMap.insert(newPair);
        return true;
    }
}

void structMap::addNew(long long price, long long amount){
    auto it = myMap.find(price);

    //if such a price already exists
    if (it != myMap.end()) {
        it->second = amount;
    }
        //if doesn't exist
    else {
        pair<long long, long long> newPair = make_pair(price, amount);
        myMap.insert(newPair);
    }
}

void structMap::deletePair(long long price) {
    myMap.erase(price);
//    cout << "Delete: " << price <<endl;
}

long long structMap::get_sum_amount() {
    long long sum_amount = 0;
    for (auto &it:myMap) {
        sum_amount = sum_amount + it.second;
    }
    cout << "Sum amount: " << sum_amount <<endl;
    return sum_amount;
}

long long structMap::get(long long price) {
    auto it = myMap.find(price);
    long long findAmount = it->second;
//    cout << "Find amount: " << findAmount <<endl;
    return findAmount;
}

pair<long long, long long> structMap::get_first() {
    auto it = myMap.begin();
    pair<long long, long long> firstPair = make_pair(it->first, it->second);
//    cout << "First pair: " << it->first << " " << it->second <<endl;
    return firstPair;
}

vector<std::pair<long long, long long>> structMap::topn(int n) {
    //copy the necessary part
    vector<std::pair<long long, long long>> nPairs;
    auto it = myMap.begin();
    int count = 0;

//    cout << "Top pairs:" <<endl;
    while (count < n){
        nPairs.emplace_back((*it));
//        cout << it->first << " " << it->second <<endl;
        it++;
        count++;
    }

    return nPairs;
}

