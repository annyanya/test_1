#include <iostream>
#include <vector>
#include <algorithm>
#include "structArrayPairs.h"

using namespace std;

structArrayPairs::structArrayPairs(){}

bool structArrayPairs::addFromFile(long long price, long long amount) {
    bool flag = false;
    long long findPrice = 0;

    //check if this element is in the array
    for (auto it = structMasPair.begin(); it != structMasPair.end(); it++) {
        if (it->first == price) {
            flag = true;
            break;
        }
    }
    if (!flag) {
        //add pair to array
        structMasPair.emplace_back(make_pair(price, amount));
        sort(structMasPair.begin(), structMasPair.end());
        return  true;
    } else {
        return  false;
    }
}

void structArrayPairs::addNew(long long price, long long amount) {
    bool flag = false;
    long long findPrice = 0;

    //check if this element is in the array
    for (auto it = structMasPair.begin(); it != structMasPair.end(); it++) {
        if (it->first == price) {
            it->second = amount;   //if there is a change in the amount
            flag = true;
            break;
        }
    }
    if (!flag) {
        //add pair to array
        structMasPair.emplace_back(make_pair(price, amount));
        sort(structMasPair.begin(), structMasPair.end());
    }
}

void structArrayPairs::deletePair(long long price) {

    for (auto it = structMasPair.begin(); it != structMasPair.end(); it++) {
        if (it->first == price) {
//           cout << "Delete: " << it->first <<endl;
            structMasPair.erase(it);
            break;
        }
    }
}

long long structArrayPairs::get_sum_amount() {
    long long sum_amount = 0;
    for (auto &it:structMasPair) {
        sum_amount = sum_amount + it.second;
    }
//    cout << "Sum amount: " << sum_amount <<endl;
    return sum_amount;
}

long long structArrayPairs::get(long long price) {
    long long findAmount = 0;
    for (auto it = structMasPair.begin(); it != structMasPair.end(); it++ ) {
        if (it->first == price) {
            findAmount = it->second;
//            cout << "Find amount: " << findAmount <<endl;
            break;
        }
    }
    return findAmount;
}

pair<long long, long long> structArrayPairs::get_first() {
    auto it = structMasPair.begin();
    pair<long long, long long> firstPair = make_pair(it->first, it->second);
//    cout << "First pair: " << it->first << " " << it->second <<endl;
    return firstPair;
}

vector<pair<long long, long long>> structArrayPairs::topn(int n) {
    //cut the necessary part
    vector<pair<long long, long long>> nPairs(structMasPair.begin(), structMasPair.begin() + n);

//    cout << "Top pairs:" <<endl;
//    for (auto &it:nPairs) {
//        cout << it.first << " " << it.second <<endl;
//    }

    return nPairs;
}
