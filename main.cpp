#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <chrono>
#include <vector>
#include <algorithm>
#include "structArrayPairs.h"
#include "structMap.h"
#include "structTree.h"

using namespace std;

void getDataArray (int numberDataset, structArrayPairs &structAP) {
    long long price, amount;

    string pathToDirectory = "/home/anna/CLionProjects/test_8/Archive/"; //path to the archive with dataset
    string nameFileDataset = pathToDirectory + "dataset" + to_string(numberDataset) + ".log";

    char c;
    vector<char> buffer;
    map <long, long> allPricesForFind;
    ifstream file(nameFileDataset,std::ios::ate);
    std::streampos size = file.tellg();


    int count = 0;
    int countAdd = 0;
    int line = 0;
    cout << "Start add data in array of pairs" << endl;

    //read the file from the end
    for(long i = 1; i <= size; i++) {
        file.seekg(-i, std::ios::end);
        file.get(c);
        if (isdigit(c)) {
            buffer.push_back(c);
        }
        else {
            count++;
            reverse(buffer.begin(), buffer.end());
            string str(buffer.begin(), buffer.end());

            if ((count%2) == 0 ) {
                amount = stoll(str);
            }
            else {
                if (count == 1) {
                    buffer.clear();
                    continue;
                }
                else {
                    line++;
                    price = stoll(str);
//                    cout << "Find: " << price << " " << amount << endl;
                    if (amount > 0){
                        // test array of pairs
                        if(structAP.addFromFile(price, amount)) {
                            countAdd++;
//                            cout << "Add: " << price << " " << amount << endl;
                            pair<long long, long long> firstPair = structAP.get_first();
                            long long sumAmounts = structAP.get_sum_amount();
                            if ((countAdd % 2) == 0) {
                                structAP.get(price);
                            }
                            if ((countAdd % 20) == 0) {
                                vector<pair<long long, long long>> topPairs = structAP.topn(10);
                            }
                        }
                    }
                }
            }
            buffer.clear();
        }
        if (line == 100000) {
            break;
        }
    }

    cout << "Count: " << countAdd << endl;
    cout << "Stop add data" << endl;
    file.close();
}

void getDataMap (int numberDataset, structMap &structM) {
    long long price, amount;

    string pathToDirectory = "/home/anna/CLionProjects/test_8/Archive/"; //path to the archive with dataset
    string nameFileDataset = pathToDirectory + "dataset" + to_string(numberDataset) + ".log";

    char c;
    vector<char> buffer;
    map <long, long> allPricesForFind;
    ifstream file(nameFileDataset,std::ios::ate);
    std::streampos size = file.tellg();


    int count = 0;
    int countAdd = 0;
    int line = 0;
    cout << "Start add data in map" << endl;

    //read the file from the end
    for(long i = 1; i <= size; i++) {
        file.seekg(-i, std::ios::end);
        file.get(c);
        if (isdigit(c)) {
            buffer.push_back(c);
        }
        else {
            count++;
            reverse(buffer.begin(), buffer.end());
            string str(buffer.begin(), buffer.end());

            if ((count%2) == 0 ) {
                amount = stoll(str);
            }
            else {
                if (count == 1) {
                    buffer.clear();
                    continue;
                }
                else {
                    price = stoll(str);
                    line++;
//                    cout << "Find: " << price << " " << amount << endl;
                    if (amount > 0){
                        // test array of pairs
                        if(structM.addFromFile(price, amount)) {
                            countAdd++;
//                            cout << "Add: " << price << " " << amount << endl;
                            pair<long long, long long> firstPair = structM.get_first();
                            long long sumAmounts = structM.get_sum_amount();
                            if ((countAdd % 2) == 0) {
                                structM.get(price);
                            }
                            if ((countAdd % 20) == 0) {
                                vector<pair<long long, long long>> topPairs = structM.topn(10);
                            }
                        }
                    }
                }
            }
            buffer.clear();
        }
        if (line == 100000) {
            break;
        }
    }

    cout << "Count: " << countAdd << endl;
    cout << "Stop add data" << endl;
    file.close();
}

void getDataTree (int numberDataset, structTree &structTr) {
    long long price, amount;

    string pathToDirectory = "/home/anna/CLionProjects/test_8/Archive/"; //path to the archive with dataset
    string nameFileDataset = pathToDirectory + "dataset" + to_string(numberDataset) + ".log";

    char c;
    vector<char> buffer;
    map <long, long> allPricesForFind;
    ifstream file(nameFileDataset,std::ios::ate);
    std::streampos size = file.tellg();


    int count = 0;
    int countAdd = 0;
    int line = 0;
    cout << "Start add data in tree" << endl;

    //read the file from the end
    for(long i = 1; i <= size; i++) {
        file.seekg(-i, std::ios::end);
        file.get(c);
        if (isdigit(c)) {
            buffer.push_back(c);
        }
        else {
            count++;
            reverse(buffer.begin(), buffer.end());
            string str(buffer.begin(), buffer.end());

            if ((count%2) == 0 ) {
                amount = stoll(str);
            }
            else {
                if (count == 1) {
                    buffer.clear();
                    continue;
                }
                else {
                    price = stoll(str);
                    line ++;
//                    cout << "Find: " << price << " " << amount << endl;
                    if (amount > 0){
                        // test array of pairs
                        if(structTr.addFromFile(price, amount)) {
                            countAdd++;
//                            cout << "Add: " << price << " " << amount << endl;
                            pair<long long, long long> firstPair = structTr.get_first();
                            long long sumAmounts = structTr.get_sum_amount();
                            if ((countAdd % 2) == 0) {
                                structTr.get(price);
                            }
                            if ((countAdd % 20) == 0) {
                                vector<pair<long long, long long>> topPairs = structTr.topn(10);
                            }
                        }
                    }
                }
            }
            buffer.clear();
        }
        if (line == 100000) {
            break;
        }
    }

    cout << "Count: " << countAdd << endl;
    cout << "Stop add data" << endl;
    file.close();
}

int main() {
    int numberDataset = 3; //database number //77114238498438
    structArrayPairs structAP;
    structMap structM;
    structTree structTr;

    //--------test for function----------------------------------------------------------------------------------------
    //--------array of pairs-------------------------------------------------------------------------------------------
//    cout << "Struct #1 - array of pairs" << endl;
//    auto start = chrono::system_clock::now();
//    getDataArray(numberDataset, structAP); //reading data from a file and entering it into a structure
//    auto end = chrono::system_clock::now();
//    cout << "TIME getData(): " <<chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;
//
//
//    start = chrono::system_clock::now();
//    long long sumAmounts = structAP.get_sum_amount();
//    end = chrono::system_clock::now();
//    cout << "TIME get_sum_amount(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    start = chrono::system_clock::now();
//    long long getAmount = structAP.get(10003);
//    end = chrono::system_clock::now();
//    cout << "TIME get(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    start = chrono::system_clock::now();
//    pair<long long, long long> firstPair = structAP.get_first();
//    end = chrono::system_clock::now();
//    cout << "TIME get_first(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    start = chrono::system_clock::now();
//    structAP.addNew(105, 8888);
//    end = chrono::system_clock::now();
//    cout << "TIME add(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    start = chrono::system_clock::now();
//    structAP.deletePair(105);
//    end = chrono::system_clock::now();
//    cout << "TIME deletePair(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    long count = 0;
//    for (int i = 0; i<100; i++) {
//        start = chrono::system_clock::now();
//        vector<pair<long long, long long>> topPairs = structAP.topn(i);
//        end = chrono::system_clock::now();
//        count = count + chrono::duration_cast<chrono::nanoseconds>(end - start).count();
//    }
//    cout << "TIME topn(): " << (count/100) << endl;

    //--------map-------------------------------------------------------------------------------------------
    cout << "Struct #2 - map" << endl;
    auto start = chrono::system_clock::now();
    getDataMap(numberDataset, structM); //reading data from a file and entering it into a structure
    auto end = chrono::system_clock::now();
    cout << "TIME getData(): " <<chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;


    start = chrono::system_clock::now();
    long long sumAmounts = structM.get_sum_amount();
    end = chrono::system_clock::now();
    cout << "TIME get_sum_amount(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;

    start = chrono::system_clock::now();
    long long getAmount = structM.get(108113227);
    end = chrono::system_clock::now();
    cout << "TIME get(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;

    start = chrono::system_clock::now();
    pair<long long, long long> firstPair = structM.get_first();
    end = chrono::system_clock::now();
    cout << "TIME get_first(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;

    start = chrono::system_clock::now();
    structM.addNew(100005, 8888);
    end = chrono::system_clock::now();
    cout << "TIME add(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;

    start = chrono::system_clock::now();
    structM.deletePair(100005);
    end = chrono::system_clock::now();
    cout << "TIME deletePair(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;

    long count = 0;
    for (int i = 0; i<100; i++) {
        start = chrono::system_clock::now();
        vector<pair<long long, long long>> topPairs = structM.topn(i);
        end = chrono::system_clock::now();
        count = count + chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    }
    cout << "TIME topn(): " << (count/100) << endl;


    //--------balanced binary tree--------------------------------------------------------------------------------------
//    cout << "Struct #3 -balanced binary tree" << endl;
//
//    auto start = chrono::system_clock::now();
//    getDataTree(numberDataset, structTr); //reading data from a file and entering it into a structure
//    auto end = chrono::system_clock::now();
//    cout << "TIME getData(): " <<chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;
//
//    start = chrono::system_clock::now();
//    long long sumAmounts = structTr.get_sum_amount();
//    end = chrono::system_clock::now();
//    cout << "TIME get_sum_amount(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    start = chrono::system_clock::now();
//    long long getAmount = structTr.get(10450);
//    end = chrono::system_clock::now();
//    cout << "TIME get(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    start = chrono::system_clock::now();
//    structTr.addNew(105, 8888);
//    end = chrono::system_clock::now();
//    cout << "TIME add(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    start = chrono::system_clock::now();
//    structTr.deletePair(10000);
//    end = chrono::system_clock::now();
//    cout << "TIME deletePair(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    start = chrono::system_clock::now();
//    pair<long long, long long> firstPair = structTr.get_first();
//    end = chrono::system_clock::now();
//    cout << "TIME get_first(): " <<chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    start = chrono::system_clock::now();
//    vector<pair<long long, long long>> topPairs = structTr.topn(5);
//    end = chrono::system_clock::now();
//    cout << "TIME topn(): " << chrono::duration_cast<chrono::nanoseconds>(end-start).count() << endl;
//
//    return 0;
}

