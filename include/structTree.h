//---------------------------------------------------------------------------------------------------------------------
//За основу взято дерево - https://habr.com/ru/post/337594/
//---------------------------------------------------------------------------------------------------------------------
#include <iostream>
#include <vector>
#include "abstractStructure.h"
#ifndef UNTITLED_STRUCTTREE_H
#define UNTITLED_STRUCTTREE_H

const int t=2;
struct BNode {
    long long keys[2*t];
    long long amounts[2*t];
    BNode *children[2*t+1];
    BNode *parent;
    int count; //количество ключей
    int countSons; //количество сыновей
    bool leaf;
};

struct ForSearch {
    bool flag;
    BNode pointer;
    int position;
    long long price;
    long long amount;
};

class structTree : public abstractStructure {
private:
    BNode *root;
    void insert_to_node(long long key, long long amount, BNode *node);
    void sort(BNode *node);
    void restruct(BNode *node);
    void deletenode(BNode *node);
    ForSearch searchKey(long long key, BNode *node);
    void remove(long long key, BNode *node);
    void removeFromNode(long long key, BNode *node);
    void removeLeaf(long long key, BNode *node);
    void lconnect(BNode *node, BNode *othernode);
    void rconnect(BNode *node, BNode *othernode);
    void repair(BNode *node);
    long long get_sum_amount_inside(BNode *node);
    std::vector<std::pair<long long, long long>> topn_inside(BNode *node);

public:
    structTree();
    ~structTree() override;
    void insert(long long key, long long amount);
    ForSearch search(long long key);
    void remove(long long key);
    bool addFromFile(long long price, long long amount) override;
    void addNew(long long price, long long amount) override ;
    void deletePair(long long price) override;
    long long get_sum_amount() override;
    long long get(long long price) override;
    std::pair<long long, long long> get_first() override;
    std::vector<std::pair<long long, long long>> topn(int n) override;

};

#endif //UNTITLED_STRUCTTREE_H
