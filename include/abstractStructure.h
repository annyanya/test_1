#include <iostream>
#include <vector>
#ifndef UNTITLED_ABSTRACTSTRUCTURE_H
#define UNTITLED_ABSTRACTSTRUCTURE_H

class abstractStructure {
public:
    virtual bool addFromFile(long long price, long long amount) = 0;
    virtual void addNew(long long price, long long amount) = 0;
    virtual void deletePair(long long price) = 0;
    virtual long long get_sum_amount() = 0;
    virtual long long get(long long price) = 0;
    virtual std::pair<long long, long long> get_first() = 0;
    virtual std::vector<std::pair<long long, long long>> topn(int n) = 0;
    virtual ~abstractStructure();
};

#endif //UNTITLED_ABSTRACTSTRUCTURE_H
