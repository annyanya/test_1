#include <map>
#include "abstractStructure.h"
#ifndef UNTITLED_STRUCTMAP_H
#define UNTITLED_STRUCTMAP_H

class structMap : public abstractStructure {
private:
    std::map<long long, long long> myMap;
public:
    structMap();
    bool addFromFile(long long price, long long amount) override;
    void addNew(long long price, long long amount) override ;
    void deletePair(long long price) override;
    long long get_sum_amount() override;
    long long get(long long price) override;
    std::pair<long long, long long> get_first() override;
    std::vector<std::pair<long long, long long>> topn(int n) override;
};

#endif //UNTITLED_STRUCTMAP_H
