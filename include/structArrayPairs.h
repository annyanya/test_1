#include <vector>
#include "abstractStructure.h"
#ifndef UNTITLED_STRUCTARRAYPAIRS_H
#define UNTITLED_STRUCTARRAYPAIRS_H

class structArrayPairs : public abstractStructure {
private:
    std::vector<std::pair<long long, long long>> structMasPair;

public:
    structArrayPairs();
    bool addFromFile(long long price, long long amount) override;
    void addNew(long long price, long long amount) override ;
    void deletePair(long long price) override;
    long long get_sum_amount() override;
    long long get(long long price) override;
    std::pair<long long, long long> get_first() override;
    std::vector<std::pair<long long, long long>> topn(int n) override;
};


#endif //UNTITLED_STRUCTARRAYPAIRS_H
