#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include "structTree.h"

using namespace std;

structTree::structTree() { root = nullptr; }
structTree::~structTree(){ if(root!=nullptr) deletenode(root); }

void structTree::deletenode(BNode *node){
    if (node!=nullptr){
        for (int i=0; i<=(2*t-1); i++){
            if (node->children[i]!=nullptr) deletenode(node->children[i]);
            else {
                delete(node);
                break;
            }
        }
    }
}

void structTree::insert_to_node(long long key, long long amount, BNode *node){
    node->keys[node->count] = key;
    node->amounts[node->count] = amount;
    node->count=node->count+1;
    sort(node);
}

void structTree::sort(BNode *node) {
    long long m;
    long long n;
    for (int i=0; i<(2*t-1); i++){
        for (int j=i+1; j<=(2*t-1); j++){
            if ((node->keys[i]!=0) && (node->keys[j]!=0)){
                if ((node->keys[i]) > (node->keys[j])){
                    m=node->keys[i];
                    n=node->amounts[i];
                    node->keys[i]=node->keys[j];
                    node->amounts[i]=node->amounts[j];
                    node->keys[j]=m;
                    node->amounts[j]=n;
                }
            } else break;
        }
    }
}

void structTree::insert(long long key, long long amount){
    if (root==nullptr) {
        BNode *newRoot = new BNode;
        newRoot->keys[0]=key;
        newRoot->amounts[0]=amount;

        for(int j=1; j<=(2*t-1); j++) {
            newRoot->keys[j]=0;
            newRoot->amounts[j]=0;
        }
        for (int i=0; i<=(2*t); i++) newRoot->children[i]=nullptr;

        newRoot->count=1;
        newRoot->countSons=0;
        newRoot->leaf=true;
        newRoot->parent=nullptr;
        root=newRoot;
    } else {
        BNode *ptr=root;
        while (!ptr->leaf){ //выбор ребенка до тех пор, пока узел не будет являться листом
            for (int i=0; i<=(2*t-1); i++){ //перебираем все ключи
                if (ptr->keys[i]!=0) {
                    if (key < ptr->keys[i]) {
                        ptr=ptr->children[i];
                        break;
                    }
                    if ((ptr->keys[i+1]==0)&&(key > ptr->keys[i])) {
                        ptr=ptr->children[i+1]; //перенаправляем к самому последнему ребенку, если цикл не "сломался"
                        break;
                    }
                } else break;
            }
        }
        insert_to_node(key, amount, ptr);

        while (ptr->count==2*t){
            if (ptr==root){
                restruct(ptr);
                break;
            } else {
                restruct(ptr);
                ptr=ptr->parent;
            }
        }
    }
}

void structTree::restruct(BNode *node){
    if (node->count<(2*t-1)) return;

    //первый сын
    BNode *child1 = new BNode;
    int j;
    for (j=0; j<=t-2; j++) {
        child1->keys[j]=node->keys[j];
        child1->amounts[j]=node->amounts[j];
    }
    for (j=t-1; j<=(2*t-1); j++) {
        child1->keys[j]=0;
        child1->amounts[j]=0;
    }
    child1->count=t-1; //количество ключей в узле
    if(node->countSons!=0){
        for (int i=0; i<=(t-1); i++){
            child1->children[i]=node->children[i];
            child1->children[i]->parent=child1;
        }
        for (int i=t; i<=(2*t); i++) child1->children[i]=nullptr;
        child1->leaf=false;
        child1->countSons=t-1; //количество сыновей
    } else {
        child1->leaf=true;
        child1->countSons=0;
        for (int i=0; i<=(2*t); i++) child1->children[i]=nullptr;
    }

    //второй сын
    BNode *child2 = new BNode;
    for (int j=0; j<=(t-1); j++) {
        child2->keys[j]=node->keys[j+t];
        child2->amounts[j]=node->amounts[j+t];
    }
    for (j=t; j<=(2*t-1); j++) {
        child2->keys[j]=0;
        child2->amounts[j]=0;
    }
    child2->count=t; //количество ключей в узле
    if(node->countSons!=0) {
        for (int i=0; i<=(t); i++){
            child2->children[i]=node->children[i+t];
            child2->children[i]->parent=child2;
        }
        for (int i=t+1; i<=(2*t); i++) child2->children[i]=nullptr;
        child2->leaf=false;
        child2->countSons=t; //количество сыновей
    } else {
        child2->leaf=true;
        child2->countSons=0;
        for (int i=0; i<=(2*t); i++) child2->children[i]=nullptr;
    }

    //родитель
    if (node->parent==nullptr){ //если родителя нет, то это корень
        node->keys[0]=node->keys[t-1];
        node->amounts[0]=node->amounts[t-1];
        for(int j=1; j<=(2*t-1); j++) {
            node->keys[j]=0;
            node->amounts[j]=0;
        }
        node->children[0]=child1;
        node->children[1]=child2;
        for(int i=2; i<=(2*t); i++) node->children[i]=nullptr;
        node->parent=nullptr;
        node->leaf=false;
        node->count=1;
        node->countSons=2;
        child1->parent=node;
        child2->parent=node;
    } else {
        insert_to_node(node->keys[t-1], node->amounts[t-1], node->parent);
        for (int i=0; i<=(2*t); i++){
            if (node->parent->children[i]==node) node->parent->children[i]=nullptr;
        }
        for (int i=0; i<=(2*t); i++){
            if (node->parent->children[i]==nullptr) {
                for (int j=(2*t); j>(i+1); j--) node->parent->children[j]=node->parent->children[j-1];
                node->parent->children[i+1]=child2;
                node->parent->children[i]=child1;
                break;
            }
        }
        child1->parent=node->parent;
        child2->parent=node->parent;
        node->parent->leaf=false;
        delete node;
    }
}

ForSearch structTree::search(long long key){
    return searchKey(key, this->root);
}

ForSearch structTree::searchKey(long long key, BNode *node){
    ForSearch res;
    if (node!=nullptr){
        if (!node->leaf){
            int i;
            for (i=0; i<=(2*t-1); i++){
                if (node->keys[i]!=0) {
                    if(key==node->keys[i]) {
                        res.flag = true;
                        res.pointer = *node;
                        res.position = i;
                        res.price = node->keys[i];
                        res.amount = node->amounts[i];
                        return res;
                    }
                    if ((key<node->keys[i])){
                        return searchKey(key, node->children[i]);
                        break;
                    }
                } else break;
            }
            return searchKey(key, node->children[i]);
        } else {
            for(int j=0; j<=(2*t-1); j++)
                if (key==node->keys[j]) {
                    res.flag = true;
                    res.pointer = *node;
                    res.position = j;
                    res.price = node->keys[j];
                    res.amount = node->amounts[j];
                    return res;
                }
            res.flag = false;
            res.pointer = *node;
            res.position = 0;
            res.price = 0;
            res.amount = 0;
            return  res;
        }
    } else {
        res.flag = false;
        //res.pointer = *node;
        res.position = 0;
        res.price = 0;
        res.amount = 0;
        return  res;
    }
}

void structTree::removeFromNode(long long key, BNode *node){
    for (int i=0; i<node->count; i++){
        if (node->keys[i]==key){
            for (int j=i; j<node->count; j++) {
                node->keys[j]=node->keys[j+1];
                node->amounts[j]=node->amounts[j+1];
                node->children[j]=node->children[j+1];
            }
            node->keys[node->count-1]=0;
            node->amounts[node->count-1]=0;
            node->children[node->count-1]=node->children[node->count];
            node->children[node->count]=nullptr;
            break;
        }
    }
    node->count--;
}

void structTree::lconnect(BNode *node, BNode *othernode){
    if (node==nullptr) return;
    for (int i=0; i<=(othernode->count-1); i++){
        node->keys[node->count]=othernode->keys[i];
        node->amounts[node->count]=othernode->amounts[i];
        node->children[node->count]=othernode->children[i];
        node->count=node->count+1;
    }
    node->children[node->count]=othernode->children[othernode->count];
    for (int j=0; j<=node->count; j++){
        if (node->children[j]==nullptr) break;
        node->children[j]->parent=node;
    }
    delete othernode;
}

void structTree::rconnect(BNode *node, BNode *othernode){
    if (node==nullptr) return;
    for (int i=0; i<=(othernode->count-1); i++){
        node->keys[node->count]=othernode->keys[i];
        node->amounts[node->count]=othernode->amounts[i];
        node->children[node->count+1]=othernode->children[i+1];
        node->count=node->count+1;
    }
    for (int j=0; j<=node->count; j++){
        if (node->children[j]==nullptr) break;
        node->children[j]->parent=node;
    }
    delete othernode;
}

void structTree::repair(BNode *node){
    if ((node==root)&&(node->count==0)){
        if (root->children[0]!=nullptr){
            root->children[0]->parent=nullptr;
            root=root->children[0];
        } else {
            delete root;
        }
        return;
    }
    BNode *ptr=node;
    long long k1, a1;
    long long k2, a2;
    int positionSon;
    BNode *parent=ptr->parent;
    for (int j=0; j<=parent->count; j++){
        if (parent->children[j]==ptr){
            positionSon=j; //позиция узла по отношению к родителю
            break;
        }
    }
    //если ptr-первый ребенок (самый левый)
    if (positionSon==0){
        insert_to_node(parent->keys[positionSon], parent->amounts[positionSon], ptr);
        lconnect(ptr, parent->children[positionSon+1]);
        parent->children[positionSon+1]=ptr;
        parent->children[positionSon]=nullptr;
        removeFromNode(parent->keys[positionSon], parent);
        if(ptr->count==2*t){
            while (ptr->count==2*t){
                if (ptr==root){
                    restruct(ptr);
                    break;
                } else {
                    restruct(ptr);
                    ptr=ptr->parent;
                }
            }
        } else
        if (parent->count<=(t-2)) repair(parent);
    } else {
        //если ptr-последний ребенок (самый правый)
        if (positionSon==parent->count){
            insert_to_node(parent->keys[positionSon-1], parent->amounts[positionSon-1], parent->children[positionSon-1]);
            lconnect(parent->children[positionSon-1], ptr);
            parent->children[positionSon]=parent->children[positionSon-1];
            parent->children[positionSon-1]=nullptr;
            removeFromNode(parent->keys[positionSon-1], parent);
            BNode *temp=parent->children[positionSon];
            if(ptr->count==2*t){
                while (temp->count==2*t){
                    if (temp==root){
                        restruct(temp);
                        break;
                    } else {
                        restruct(temp);
                        temp=temp->parent;
                    }
                }
            } else
            if (parent->count<=(t-2)) repair(parent);
        } else { //если ptr имеет братьев справа и слева
            insert_to_node(parent->keys[positionSon], parent->amounts[positionSon], ptr);
            lconnect(ptr, parent->children[positionSon+1]);
            parent->children[positionSon+1]=ptr;
            parent->children[positionSon]=nullptr;
            removeFromNode(parent->keys[positionSon], parent);
            if(ptr->count==2*t){
                while (ptr->count==2*t){
                    if (ptr==root){
                        restruct(ptr);
                        break;
                    } else {
                        restruct(ptr);
                        ptr=ptr->parent;
                    }
                }
            } else
            if (parent->count<=(t-2)) repair(parent);
        }
    }
}

void structTree::removeLeaf(long long key, BNode *node){
    if ((node==root)&&(node->count==1)){
        removeFromNode(key, node);
        root->children[0]=nullptr;
        delete root;
        root=nullptr;
        return;
    }
    if (node==root) {
        removeFromNode(key, node);
        return;
    }
    if (node->count>(t-1)) {
        removeFromNode(key, node);
        return;
    }
    BNode *ptr=node;
    long long k1, a1;
    long long k2, a2;
    int position;
    int positionSon;
    int i;
    for (int i=0; i<=node->count-1; i++){
        if (key==node->keys[i]) {
            position=i; //позиция ключа в узле
            break;
        }
    }
    BNode *parent=ptr->parent;
    for (int j=0; j<=parent->count; j++){
        if (parent->children[j]==ptr){
            positionSon=j; //позиция узла по отношению к родителю
            break;
        }
    }
    //если ptr-первый ребенок (самый левый)
    if (positionSon==0){
        if (parent->children[positionSon+1]->count>(t-1)){ //если у правого брата больше, чем t-1 ключей
            k1=parent->children[positionSon+1]->keys[0]; //k1 - минимальный ключ правого брата
            a1=parent->children[positionSon+1]->amounts[0];
            k2=parent->keys[positionSon]; //k2 - ключ родителя, больше, чем удаляемый, и меньше, чем k1
            a2=parent->amounts[positionSon];
            insert_to_node(k2, a2, ptr);
            removeFromNode(key, ptr);
            parent->keys[positionSon]=k1; //меняем местами k1 и k2
            parent->amounts[positionSon]=a1;
            removeFromNode(k1, parent->children[positionSon+1]); //удаляем k1 из правого брата
        } else { //если у правого <u>единственного</u> брата не больше t-1 ключей
            removeFromNode(key, ptr);
            if (ptr->count<=(t-2)) repair(ptr);
        }
    } else {
        //если ptr-последний ребенок (самый правый)
        if (positionSon==parent->count){
            //если у левого брата больше, чем t-1 ключей
            if (parent->children[positionSon-1]->count>(t-1)){
                BNode *temp=parent->children[positionSon-1];
                k1=temp->keys[temp->count-1]; //k1 - максимальный ключ левого брата
                a1=temp->amounts[temp->count-1];
                k2=parent->keys[positionSon-1]; //k2 - ключ родителя, меньше, чем удаляемый и больше, чем k1
                a2=parent->amounts[positionSon-1];
                insert_to_node(k2, a2, ptr);
                removeFromNode(key, ptr);
                parent->keys[positionSon-1]=k1;
                parent->amounts[positionSon-1]=a1;
                removeFromNode(k1, temp);
            } else { //если у <u>единственного</u> левого брата не больше t-1 ключей
                removeFromNode(key, ptr);
                if (ptr->count<=(t-2)) repair(ptr);
            }
        } else { //если ptr имеет братьев справа и слева
            //если у правого брата больше, чем t-1 ключей
            if (parent->children[positionSon+1]->count>(t-1)){
                k1=parent->children[positionSon+1]->keys[0]; //k1 - минимальный ключ правого брата
                a1=parent->children[positionSon+1]->amounts[0];
                k2=parent->keys[positionSon]; //k2 - ключ родителя, больше, чем удаляемый и меньше, чем k1
                a2=parent->amounts[positionSon];
                insert_to_node(k2, a2, ptr);
                removeFromNode(key, ptr);
                parent->keys[positionSon]=k1; //меняем местами k1 и k2
                parent->amounts[positionSon]=a1;
                removeFromNode(k1, parent->children[positionSon+1]); //удаляем k1 из правого брата
            } else {
                //если у левого брата больше, чем t-1 ключей
                if (parent->children[positionSon-1]->count>(t-1)){
                    BNode *temp=parent->children[positionSon-1];
                    k1=temp->keys[temp->count-1]; //k1 - максимальный ключ левого брата
                    a1=temp->amounts[temp->count-1];
                    k2=parent->keys[positionSon-1]; //k2 - ключ родителя, меньше, чем удаляемый и больше, чем k1
                    a2=parent->amounts[positionSon-1];
                    insert_to_node(k2, a2, ptr);
                    removeFromNode(key, ptr);
                    parent->keys[positionSon-1]=k1;
                    parent->amounts[positionSon-1]=a1;
                    removeFromNode(k1, temp);
                } else { //если у обоих братьев не больше t-1 ключей
                    removeFromNode(key, ptr);
                    if (ptr->count<=(t-2)) repair(ptr);
                }
            }
        }
    }
}

void structTree::remove(long long key, BNode *node){
    BNode *ptr=node;
    int position; //номер ключа
    int i;
    for (int i=0; i<=node->count-1; i++){
        if (key==node->keys[i]) {
            position=i;
            break;
        }
    }
    int positionSon; //номер сына по отношению к родителю
    if (ptr->parent!=nullptr){
        for(int i=0; i<=ptr->parent->count; i++){
            if (ptr->children[i]==ptr){
                positionSon==i;
                break;
            }
        }
    }
    //находим наименьший ключ правого поддерева
    ptr=ptr->children[position+1];
    long long newkey=ptr->keys[0];
    long long newamount=ptr->amounts[0];
    while (ptr->leaf==false) ptr=ptr->children[0];
    //если ключей в найденном листе не больше 1 - ищем наибольший ключ в левом поддереве
    //иначе - просто заменяем key на новый ключ, удаляем новый ключ из листа
    if (ptr->count>(t-1)) {
        newkey=ptr->keys[0];
        newamount=ptr->amounts[0];
        removeFromNode(newkey, ptr);
        node->keys[position]=newkey;
        node->amounts[position]=newamount;
    } else {
        ptr=node;
        //ищем наибольший ключ в левом поддереве
        ptr=ptr->children[position];
        newkey=ptr->keys[ptr->count-1];
        newamount=ptr->amounts[ptr->count-1];
        while (ptr->leaf==false) ptr=ptr->children[ptr->count];
        newkey=ptr->keys[ptr->count-1];
        newamount=ptr->amounts[ptr->count-1];
        node->keys[position]=newkey;
        node->amounts[position]=newamount;
        if (ptr->count>(t-1)) removeFromNode(newkey, ptr);
        else {
            //если ключей не больше, чем t-1 - перестраиваем
            removeLeaf(newkey, ptr);
        }
    }
}

void structTree::remove(long long key){
    BNode *ptr=this->root;
    int position;
    int positionSon;
    int i;
    if (searchKey(key, ptr).flag==false) {
        return;
    } else {
        //ищем узел, в котором находится ключ для удаления
        for (i=0; i<=ptr->count-1; i++){
            if (ptr->keys[i]!=0) {
                if(key==ptr->keys[i]) {
                    position=i;
                    break;
                } else {
                    if ((key<ptr->keys[i])){
                        ptr=ptr->children[i];
                        positionSon=i;
                        i=-1;
                    } else {
                        if (i==(ptr->count-1)) {
                            ptr=ptr->children[i+1];
                            positionSon=i+1;
                            i=-1;
                        }
                    }
                }
            } else break;
        }
    }
    if (ptr->leaf==true) {
        if (ptr->count>(t-1)) removeFromNode(key,ptr);
        else removeLeaf(key, ptr);
    } else remove(key, ptr);
}

long long structTree::get_sum_amount_inside(BNode *node) {
    long long currentAmount = 0;

    if (node!=nullptr){
        for (int i=0; i<=(2*t-1); i++){
            currentAmount = currentAmount + node->amounts[i];
            if (node->children[i]!=nullptr) {
                currentAmount = currentAmount + get_sum_amount_inside(node->children[i]);
            }
        }
    }

    return currentAmount;
}

long long structTree::get_sum_amount() {
    BNode *node = this->root;
    long long finalSum = get_sum_amount_inside(node);
//    cout << "Sum amount: " << finalSum <<endl;
    return finalSum;
}

std::pair<long long, long long> structTree::get_first() {
    BNode *ptr=root;
    while(!ptr->leaf) {
        ptr = ptr->children[0];
    }
    long long key = ptr->keys[0];
    long long amount = ptr->amounts[0];
    pair<long long, long long> firstPair = make_pair(key, amount);
//    cout << "First pair: " << key << " " << amount <<endl;
    return firstPair;
}

vector<pair<long long, long long>> structTree::topn_inside(BNode *node) {
    vector<pair<long long, long long>> topVector;
    vector<pair<long long, long long>> addVector;
    int i = 0;
    if (!node) {
        return topVector;
    }

    if (node->children[0] != nullptr) {
        addVector = topn_inside(node->children[0]);
        topVector.insert(topVector.begin(), addVector.begin(), addVector.end());
    }

    for (int j = 0; j <= (2 * t - 1); j++) {
        if (node->keys[j] != 0) {
            pair<long long, long long> newPair = make_pair(node->keys[j], node->amounts[j]);
            topVector.push_back(newPair);
//            cout << "Top vector add: " << node->keys[j] << " " << node->amounts[j] << endl;
            i++;
        } else {
            break;
        }
    }

    for (int j = 1; j <= (2 * t - 1); j++) {
        if (node->children[j] != nullptr) {
            addVector = topn_inside(node->children[j]);
            topVector.insert(topVector.end(), addVector.begin(), addVector.end());
        }
    }

    return topVector;
}

vector<pair<long long, long long>> structTree::topn(int n) {
    BNode *ptr=root;
    vector<pair<long long, long long>> topVector;
    vector<pair<long long, long long>> finalTopVector;

    topVector = topn_inside(ptr);
    auto it = topVector.begin();
    finalTopVector.insert(finalTopVector.begin(), it, it + n);

//    cout << "Top pairs:" <<endl;
//    for (auto &vec: finalTopVector) {
//        cout << vec.first << " " << vec.second <<endl;
//    }

    return  finalTopVector;
}

bool structTree::addFromFile(long long price, long long amount) {
    ForSearch resultSearch = search(price);
    if(resultSearch.flag) {
        return false;
    }
    else {
        insert(price, amount);
        return true;
    }
}

void structTree::addNew(long long price, long long amount) {
    ForSearch resultSearch = search(price);

    if(resultSearch.flag) {
        resultSearch.pointer.amounts[resultSearch.position] = amount;
    }
    else {
        insert(price, amount);
    }
}

void structTree::deletePair(long long price) {
    remove(price);
//    cout << "Delete: " << price <<endl;
}

long long structTree::get(long long price) {
    ForSearch resultSearch = search(price);
    long long findAmount = resultSearch.amount;
//    cout << "Find amount: " << findAmount <<endl;
    return findAmount;
}


